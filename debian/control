Source: dh-ada-library
Maintainer: Nicolas Boulenguez <nicolas@debian.org>
Uploaders: Ludovic Brenta <lbrenta@debian.org>
Section: devel
Priority: optional
Standards-Version: 4.5.0
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 gnat-10,
# The following line has its role during the gnat-10 transition in
# experimental, but we keep the one without version so that the
# test can go on and extract the ALI version.
 libgnatprj8-dev (>= 2021.0.0.0778b109-2),
 libgnatprj8-dev,
# The same versions must be updated manually in tests/control.
# The test script extracts the gnatprj version from here.
Vcs-Browser: https://salsa.debian.org/debian/dh_ada_library
Vcs-Git: https://salsa.debian.org/debian/dh_ada_library.git

Package: dh-ada-library
Provides: dh-sequence-ada-library
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, ${perl:Depends},
 debhelper,
 dpkg-dev,
# Calls dpkg-architecture.
 binutils
# Calls readelf.
# Recommends: debian-ada-policy (not a .deb yet)
Recommends: gnat
Description: Debian helper for Ada libraries
 Debhelper tools to ease common tasks in packaging libraries written
 in the Ada programming language, following the Debian Policy for Ada
 (http://people.debian.org/~lbrenta/debian-ada-policy.html).
 .
 A specific gnat-X is recommended via the gnat package, but any
 version should be supported.
