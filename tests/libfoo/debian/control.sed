Source: libfoo
Priority: optional
Section: libdevel
Maintainer: Nobody <nobody@nowhere.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-ada-library,
 gnat,
 @GNAT_PKG@,
 @GPR_DEV_PKG@,
 zlib1g-dev,
Standards-Version: 4.5.0
Homepage: https://nowhere.org

Package: libfoo3-dev
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}
Description: fake foo library to test dh-ada-library (development)
 This package contains the development tools.

Package: libf-l4-dev
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}
Description: fake f-l library to test dh-ada-library (development)
 This package contains the development tools.

Package: libfoo5
Section: libs
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: fake foo library to test dh-ada-library (runtime)
 This package contains the runtime shared library.

Package: libf-l6
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: fake f-l library to test dh-ada-library (runtime)
 This package contains the runtime shared library.
