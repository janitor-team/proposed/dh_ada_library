.\"  French manual page for dh_ada_library
.\"
.\"  Copyright (C) 2012-2020 Nicolas Boulenguez <nicolas@debian.org>
.\"
.\"  This program is free software: you can redistribute it and/or
.\"  modify it under the terms of the GNU General Public License as
.\"  published by the Free Software Foundation, either version 3 of the
.\"  License, or (at your option) any later version.
.\"  This program is distributed in the hope that it will be useful, but
.\"  WITHOUT ANY WARRANTY; without even the implied warranty of
.\"  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
.\"  General Public License for more details.
.\"  You should have received a copy of the GNU General Public License
.\"  along with this program. If not, see <http://www.gnu.org/licenses/>.
.\"
.TH DH_ADA_LIBRARY 1 "2020-04-21"
.\"----------------------------------------------------------------------
.SH NAME
dh_ada_library \- aide à empaqueter des bibliothèques Ada pour Debian
.\"----------------------------------------------------------------------
.SH SYNOPSIS
.B dh_ada_library
.RI [\| "options debhelper" \|]
.RI [\|[\| variable = valeur " .\|.\|.\|] " projet ".gpr\|] .\|.\|."
.\"----------------------------------------------------------------------
.SH DESCRIPTION
.B dh_ada_library
est un programme de la suite debhelper prenant en charge quelques
tâches courantes dans l’empaquetage de bibliothèques écrites dans le
langage Ada.
.PP
Chaque argument
.I projet.gpr
indique un fichier de projet GNAT servant à compiler une des
bibliothèques partagées fournies par le paquet source.
.\"
L’usage de variables d’environnement dans de tels projets étant
monnaie courante, il est possible de fournir une liste d’affectations
avant chaque projet.
.PP
\fBdh_ada_library\fR lit chaque fichier de projet et en extrait les
informations suivantes :
le nom de la bibliothèque,
les projets importés,
les répertoires contenant les sources,
le répertoire contenant la bibliothèque partagée
(\fILibrary_Dir\fR),
celui contenant les fichiers ALI
(\fIObject_Dir\fR),
et les options de l’éditeur de liens
(\fILinker'Linker_Options\fR).
.\"
Ensuite, il parcourt
.I debian/control
afin de vérifier que des paquets existent, nommés conformément à la
\fBCharte Debian pour Ada\fR
sous l’intitulé \fICoexistence Not Allowed\fR
et il en déduit le numéro de version des fichiers ALI
(\fIaliversion\fR) et de
la bibliothèque partagée (\fIsoversion\fR).
.\"----------------------------------------------------------------------
.SS Paquet fournissant la bibliothèque dynamique
\fBdh\-ada\-library\fR installe la bibliothèque partagée dans ce paquet.
.PP
Ceci devrait être inutile depuis \fBgnat\-7\fR, mais si la
bibliothèque partagée déclare sa pile exécutable,
\fBdh_ada_library\fR génère un « override » lintian expliquant que
GNAT utilise des trampolines pour traiter les exceptions.
.\"
Si \fBdh\-ada\-library\fR est appelé directement
sans passer par le séquenceur \fBdh\fR
ni la cible \fIoverride_dh_ada_library\fR,
il faut s’assurer que
.B dh_lintian
est exécuté, car s’il l’était plus tard il risquerait de recouvrir le
fichier « override ».
.\"----------------------------------------------------------------------
.SS Paquet de développement (\-dev)
Pour commencer, \fBdh_ada_library\fR installe le lien symbolique de
développement \fIlibLIBRARY.so\fR pointant vers
\fIlibLIBRARY.so.soversion\fR.
.PP
Les fichiers ALI y sont installés en lecture seule (mode 444), sans
les lignes mentionant les options de construction
\fI\-f*\-prefix\-map\fR (améliorant la
reproductibilité, en attendant que la solution correcte
BUILD_PATH_PREFIX_MAP soit acceptée dans GCC).
.PP
Les sources sont également installées, pour tous les langages connus
du projet de compilation.
.PP
.B dh\-ada\-library
recherche un fichier nommé
.I libLIBRARY_NAME.a
(la bibliothèque statique) dans le répertoire courant ou un de ses
sous-répertoires, et l’installe dans le paquet \-dev.
.PP
Un second fichier de projet, conçu pour faciliter la compilation de
programmes utilisant la bibliothèque, est généré et installé dans le
paquet \-dev,
qui copie les exceptions de renommage du projet d’origine.
.\"
Il copie également les options \fILinker'Linker_Switches\fR de
l’éditeur de lien (voir plus loin).
.PP
La variable de substitution
.I ada:Depends
reçoit une valeur indiquant que le paquet dépend de
.IR gnat ,
de
.I gnat\-X
et du paquet de bibliothèque.
.\"
Pour chaque projet de bibliothèque importé et reconnu,
soit comme déjà parcouru,
soit comme installé par les \fIBuild\-Depends\-Arch\fR,
une dépendance est ajoutée dans \fIada:Depends\fR
et le projet généré se voit ajouter une ligne \fIwith\fR.
.\"
Dans le cas d’un projet déjà parcouru dans le cadre du même paquet
source,
la dépendance exige exactement la même \fIbinary:Version\fR,
afin de garantir que toutes les archives statiques sont compilées
avec des options compatibles.
.\"
Lorsqu’une option dans la liste \fILinker'Linker_Options\fR du projet
de construction, est de la forme \fB\-l\fIFOO\fR, le lien
\fIlibFOO.so\fR est recherché dans le répertoire contenant les
bibliothèques de ce système, et le paquet responsable de son
installation est ajouté à la variable de substitution
\fIada:Depends\fR.
.\"----------------------------------------------------------------------
.SH INTÉGRATION DANS DEBHELPER
La plupart des paquets n’ont besoin que d’ajouter
\fIdh\-sequence\-ada\-library\fR à \fIBuild\-Depends\-Arch\fR dans
\fIdebian/control\fR.
.\"
Debhelper (>= 12) détectera la dépendance et exécutera
\fBdh_ada_library\fR après \fBdh_lintian\fR lors des séquences
\fIbinary\-arch\fR et \fIbinary\fR.
.PP
Certains paquets ne distinguent pas \fIBuild\-Depends\-Arch\fR, par
exemple parce qu’ils ne construisent que des paquets dépendant de
l’architecture.
.\"
En ce cas, il est pratique d’ajouter plutôt la dépendance à
\fIBuild\-Depends\fR.
.\"
La commande sera alors exécutée aussi pendant la séquence
\fIbinary\-indep\fR, quoique probablement sans effet.
.PP
Les paquets compatibles avec debhelper (<< 12) doivent ajouter
\fIdh\-ada\-library\fR à \fIBuild\-Depends\-Arch\fR, ainsi qu’un
paragraphe comme
.nf
.B override_dh_lintian-arch:
.B "        dh_lintian -a"
.B "        dh_ada_library"
.fi
dans \fIdebian/rules\fR.
.PP
Les paquets compatibles avec debhelper (<< 12) mais ne distinguant pas
\fIBuild\-Depends\-Arch\fR peuvent simplement ajouter
\fIdh\-ada\-library\fR à \fIBuild\-Depends\fR et passer l’option
\fI\-\-with=ada\-library\fR au séquenceur \fBdh\fR dans
\fIdebian/rules\fR.
.\"----------------------------------------------------------------------
.SH REMARQUES
L’attribut
.I Library_Version
du projet est délibérément ignoré, et le nom d’objet partagé
(\fIsoname\fR) est plutôt déduit à partir du nom du paquet de
bibliothèque.
.\"
Les projets réclamant une définition de variable externe pour définir
cet attribut pourront recevoir une valeur arbitraire.
.\"
Ceci permettra souvent au mainteneur l’usage d’un simple fichier
.I debian/ada_libraries
au lieu de surcharger une commande debhelper ou d’exporter une
variable d’environnement.
.PP
Les deux bibliothèques et les fichiers ALI sont installés dans
.IR /usr/lib/DEB_HOST_MULTIARCH ,
le projet et les sources dans
.IR /usr/share/ada/adainclude .
.\"
Ceci signifie que le paquet \-dev ne peut pas être déclaré
.I Multi-Arch: same
puisque le contenu du projet (et le cas échéant de sources générées)
sera différent selon l’architecture.
.PP
L’attribut \fILinker'Linker_Options\fR devrait rarement être utile.
.\"
Une dépendance écrite en Ada gagne à être décrite dans un projet
importé, qui tiendra également compte des fichiers \fI.ali\fR.
.\"
UNe dépendance écrite en C doit figurer dans \fILibrary_Options\fR
lors de la construction d’une bibliothèque partagée, et dans
\fILinker'Linker_Options\fR lors de l’utilisation d’une bibliothèque
statique, mais n’est pas toujours utile dans
\fILinker'Linker_Options\fR pour une bibliothèque partagée.
.\"
Jusqu’à présent, ces options ne se sont avérées utiles que lorsqu’un
\fIbinding\fR léger importe des symbolse C dans une spécification ou
une procédure \fIinlinées\fR.
.\"
Dans un fichier \fIfoo.pc\fR utilisé par \fIpkg\-config\fR, il
s’agirait des \fILibs\fR, à l’exclusion de \fI\-lfoo\fR et des
\fILibs.private\fR.
.\"
Les versions antérieures à 7.0 trouvaient ces informations dans
\fILeading_Library_Options\fR ou \fILibrary_Options\fR, au lieu de
\fILinker'Linker_Options\fB.
.\"
Malheureusement, les tests d’intégration (comme \fIautopkgtests\fR)
semblent le seul moyen de détecter les paquets nécessitant encore ce
comportement.
.\"----------------------------------------------------------------------
.SH FICHIERS
.TP
.I debian/ada_libraries
Projets et variables à traiter en plus de ceux mentionnés en ligne de
commande en cas d’appel directement depuis \fIdebian/rules\fR.
.\"
Les fins de ligne sont considérés comme de simples espaces.
.\"
Ceci peut être amené à changer, et il est vivement recommandé de
mettre exactement une affectation ou un projet par ligne.
.\"
Toute ligne débutant par un dièse est ignorée.
.\"----------------------------------------------------------------------
.SH OPTIONS
Les options en ligne de commande et variables d’environnement communes
aux outils debhelper sont reconnues.
.\"----------------------------------------------------------------------
.SH EXEMPLES
dh_ada_library 'DIRS=src gen' SOVERSION=sans_effet toto.gpr \-\-verbose \-\-no\-act
.\"----------------------------------------------------------------------
.SH VERSIONS
La version comporte deux nombres séparés par un point.
.\"
Le second varie lors d’une mise à jour normale, qui est censée
corriger des bugs, introduire des évolutions compatibles ou supprimer
des services antérieurs à \fIoldoldstable\fR.
.\"
Une variation du premier annonce une incompatibilité obligeant
certains utilisateurs à modifier leur paquet source.
.\"
.\"----------------------------------------------------------------------
.SH VOIR AUSSI
.BR debhelper (7),
.BR dh_installdocs (1),
.BR dh_lintian (1),
.BR deb\-substvars (5),
la
.B Charte Debian pour Ada
disponible (en anglais) à l’adresse
.IR http://people.debian.org/~lbrenta/debian\-ada\-policy.html .
.\"----------------------------------------------------------------------
.SH AUTEUR
Nicolas Boulenguez <nicolas@debian.org> a écrit
.B dh_ada_library
et sa page de manuel à destination du projet Debian et de quiconque
les trouvera utiles.
