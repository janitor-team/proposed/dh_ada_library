.\"  English manual page for dh_ada_library
.\"
.\"  Copyright (C) 2012-2020 Nicolas Boulenguez <nicolas@debian.org>
.\"
.\"  This program is free software: you can redistribute it and/or
.\"  modify it under the terms of the GNU General Public License as
.\"  published by the Free Software Foundation, either version 3 of the
.\"  License, or (at your option) any later version.
.\"  This program is distributed in the hope that it will be useful, but
.\"  WITHOUT ANY WARRANTY; without even the implied warranty of
.\"  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
.\"  General Public License for more details.
.\"  You should have received a copy of the GNU General Public License
.\"  along with this program. If not, see <http://www.gnu.org/licenses/>.
.\"
.TH DH_ADA_LIBRARY 1 "2020-04-21"
.\"----------------------------------------------------------------------
.SH NAME
dh_ada_library \- help packaging Ada libraries for Debian
.\"----------------------------------------------------------------------
.SH SYNOPSIS
.B dh_ada_library
.RI [\| "debhelper options" \|]
.RI [\|[\| variable = value " .\|.\|.\|] " project ".gpr \|] .\|.\|."
.\"----------------------------------------------------------------------
.SH DESCRIPTION
.B dh_ada_library
is a debhelper program that handles some common tasks in packaging
libraries written in the Ada programming language.
.PP
Each
.I project.gpr
argument designates a GNAT project file building a shared library
provided by the source package.
.\"
Since using environment variables in such projects is common practice,
a list of variable assignments may be provided before each project.
.PP
\fBdh_ada_library\fR reads each project file and extracts the following
information from it:
library name,
imported projects,
source directories,
library directory,
object directory (containing ALI files)
and linker options (\fILinker'Linker_Options\fR).
.\"
Then it parses
.I debian/control
to check that package names exist conforming to the
Coexistence Not Allowed naming
scheme described in the \fBDebian Policy for Ada\fR and extracts the
ALI files version (\fIaliversion\fR)) and the
shared library version (\fIsoversion\fR).
.\"----------------------------------------------------------------------
.SS Runtime library package
\fBdh_ada_library\fR installs the shared library into this package.
.PP
This should not be necessary anymore since \Bgnat\-7\fR, but if the
shared library declares its stack executable,
\fBdh_ada_library\fR generates a lintian override for the runtime
library package about GNAT using trampolines for exception handling.
.\"
If \fBdh\-ada\-library\fR is called directly,
that is neither by the \fBdh\fR sequencer
nor by the \fIoverride_dh_ada_library\fR target,
this should be after
.B dh_lintian
so that the latter does not overwrite the override.
.\"----------------------------------------------------------------------
.SS Development package (\-dev)
First, \fBdh_ada_library\fR installs the usual development symbolic
\fIlibLIBRARY.so\fR referring to +\fIlibLIBRARY.so.soversion\fR.
.PP
The ALI files are installed in read-only mode (444),
stripped from the lines containing \fI\-f*\-prefix\-map\fR build
options are removed (improving build reproducibility, until the proper
solution BUILD_PATH_PREFIX_MAP is accepted by GCC).
.\"
All sources are also installed, from each language in the build
project.
.\"
\fBdh\-ada\-library\fR then looks for a file named
.IR lib LIBRARY_NAME .a
(the static library) in the current directory or its subdirectories
and installs this file into the \-dev package.
.PP
A second project file, intended for use by programs linking against
the library, is generated and installed into the \-dev package,
copying the renaming exceptions and \fILinker'Linker_Options\fR
attribute of the original project (see below).
.PP
The
.I ada:Depends
substitution variable receives a value reflecting that the package
depends on
.IR gnat ,
.I gnat\-X
and the library package.
.\"
For each imported library project recognized,
either as already processed
or installed by one of the \fIBuild\-Depends\fR,
a dependency is added into \fIada:Depends\fR and the generated project
is added the corresponding \fIwith\fR line.
.\"
For already processed project within the same source package,
the dependency mandates an exact \fIbinary:Version\fR,
ensuring that all static libraries are compiled with compatible options.
.\"
For each option like \fB\-l\fIFOO\fR in \fILinker'Linker_Options\fR, a
matching \fIlibFOO.so\fR is searched in the default library directory,
and the -dev package responsible for its installation is added to
\fIada:Depends\fR.
.\"----------------------------------------------------------------------
.SH DEBHELPER INTEGRATION
Most packages only need to add \fIdh\-sequence\-ada\-library\fR to
\fIBuild\-Depends\-Arch\fR in \fIdebian/control\fR.
.\"
Debhelper (>= 12) wild detect the dependency and execute
\fBdh_ada_library\fR after \fIdh_lintian\fR as part of
\fIbinary\-arch\fR and \fIbinary\fR sequences.
.PP
Some packages do not distinguish \fIBuild\-Depends\-Arch\fR, for
example because they only build architecture\-dependent binaries.
.\"
It is then convenient to add the dependency to \fIBuild\-Depends\fR
instead.
.\"
The tool will then also run as part of the \fIbinary\-indep\fR
sequence, though probably not doing anything.
.PP
Packages compatible with debhelper (<< 12) should add
\fIdh\-ada\-ada\-library\fR to \fIBuild\-Depends\-Arch\fR, and an
override like
.nf
.B override_dh_lintian-arch:
.B "        dh_lintian -a"
.B "        dh_ada_library"
.fi
into \fIdebian/rules\fR.
.PP
Packages compatible with debhelper (<< 12), but not distinguishing
\fIBuild\-Depends\-Arch\fR, may just add \fIdh\-ada\-ada\-library\fR
to \fIBuild\-Depends\fR and pass the \fI\-\-with=ada\-library\fR
option to the \fBdh\fR sequencer in \fIdebian/rules\fR.
.\"----------------------------------------------------------------------
.SH REMARKS
The
.I Library_Version
project attribute is purposedly ignored.
.\"
Instead, the soname is guessed from the library package name.
.\"
For projects needing an external variable definition to set this
attribute, any arbitrary value may be given.
.\"
This will often allow the maintainer to use a simple
.I debian/ada_libraries
file instead of a debhelper override or an environment variable
exportation.
.PP
Both libraries and ALI files are installed into
.IR /usr/lib/ DEB_HOST_MULTIARCH ,
the project and sources into
.IR /usr/share/ada/adainclude .
.\"
This implies that the \-dev package cannot be declared
.I Multi-Arch: same
as the content of the project (and maybe of generated sources) will
vary across architectures.
.PP
The \fILinker'Linker_Options\fR attribute should rarely be needed.
.\"
A dependency written in Ada should be described by an imported
project, also handling \fI.ali\fR files.
.\"
A dependency written in C should be listed in \fILibrary_Options\fR
when building a shared library, and in \fILinker'Linker_Options\fR
when using a static archive, but is not always required in
\fILinker'Linker_Options\fR for shared libraries.
.\"
Until now, such options have only proved useful to light bindings
importing C symbols from a specification or an inlined procedure.
.\"
In a \fIfoo.pc\fR file as read by \fIpkg\-config\fR, they are named
\fILibs\fR, excluding \fI\-lfoo\fR and \fILibs.private\fR.
.\"
.PP
Versions prior to 7 were reading \fILeading_Library_Options\fR and
\fILibrary_Options\fR instead of \fILinker'Linker_Options\fR.
.\"
Unfortunately, integration tests (like \fIautopkgtests\fR) seem to be
the only way to detect packages relying on this behaviour.
.\"----------------------------------------------------------------------
.SH FILES
.TP
.I debian/ada_libraries
Projects and variables, in addition to those in the command line
if called directly from \fIdebian/rules\fR.
.\"
Line breaks are considered as normal spaces.
.\"
This may change in the future and it is recommended to put exactly
one affectation or project on each line.
.\"
Any line starting with a dash will be ignored.
.\"----------------------------------------------------------------------
.SH OPTIONS
Common debhelper command line options and environment variables are
recognized.
.\"----------------------------------------------------------------------
.SH EXAMPLES
dh_ada_library 'DIRS=src gen' SOVERSION=ignored foo.gpr \-\-verbose \-\-no\-act
.\"----------------------------------------------------------------------
.SH VERSIONS
The version is composed of two numbers separated by a dot.
.\"
The second one increases during a normal update, which supposedly
fixes bugs, introduces compatible evolutions, or remove stuff unused
since \fIoldoldstable\fR.
.\"
A change in the first one announces an incompatibility requiring some
users to modify their source package.
.\"----------------------------------------------------------------------
.SH SEE ALSO
.BR debhelper (7),
.BR dh_installdocs (1),
.BR dh_lintian (1),
.BR deb\-substvars (5),
the
.B Debian Policy for Ada
available at
.IR http://people.debian.org/~lbrenta/debian\-ada\-policy.html .
.\"----------------------------------------------------------------------
.SH AUTHOR
.B dh_ada_library
and this manpage were written by Nicolas Boulenguez
<nicolas@debian.org> for the Debian project (and may be used
by others).
