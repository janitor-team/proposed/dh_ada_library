--  dh_ada_library, helper for Ada libraries Debian maintainers
--
--  Copyright (C) 2012-2020 Nicolas Boulenguez <nicolas@debian.org>
--
--  This program is free software: you can redistribute it and/or
--  modify it under the terms of the GNU General Public License as
--  published by the Free Software Foundation, either version 3 of the
--  License, or (at your option) any later version.
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
--  General Public License for more details.
--  You should have received a copy of the GNU General Public License
--  along with this program. If not, see <http://www.gnu.org/licenses/>.
--
with Ada.Directories;

with GNAT.OS_Lib;

package Misc is

   --  Various subprograms of general interest, mainly encapsulating
   --  subprocesses executions.

   pragma Elaborate_Body;

   Subprocess_Error : exception;
   Multiline_Output : exception;

   procedure Raise_Subprocess_Error
     (Program_Name : in String;
      Args         : in GNAT.OS_Lib.Argument_List;
      Return_Code  : in Integer);
   --  Output a message on Standard_Error then raise Subprocess_Error.

   procedure Capture_Output
     (Program_Name : in     String;
      Args         : in     GNAT.OS_Lib.Argument_List;
      Temp_File    :    out GNAT.OS_Lib.String_Access;
      Return_Code  :    out Integer);
   pragma Precondition (Program_Name (Program_Name'First) = '/');
   --  Temp_File is allocated the name of a freshly created file. If
   --  something unexpected happens like a full disk, then
   --  Subprocess_Error is raised, Temp_File = null and no file is
   --  created (or left).

   procedure Capture_Output
     (Program_Name : in     String;
      Args         : in     GNAT.OS_Lib.Argument_List;
      Temp_File    :    out GNAT.OS_Lib.String_Access);
   pragma Precondition (Program_Name (Program_Name'First) = '/');
   --  Calls Raise_Subprocess_Error if Return_Code /= 0.

   function A_Line_Matches (File_Name : in String;
                            Pattern   : in String)
                           return Boolean;

   function Relative_Path
     (Target           : in String;
      Source_Directory : in String := Ada.Directories.Current_Directory)
     return String;
   --  Returns the shortest Path such that Target is designated by
   --  Source_Directory & '/' & Path. In particular, returns "." if
   --  Target = Source_Directory, and a string containing zero or more
   --  "../" then normal names in other cases.

   --  The result does depend on the existence of actual files,
   --  symbolic links or directories.

   function Output_Of
     (Program_Name : in String;
      Args         : in GNAT.OS_Lib.Argument_List)
     return String;
   pragma Precondition (Program_Name (Program_Name'First) = '/');
   --  May raise Subprocess_Error or Multiline_Output.

end Misc;
