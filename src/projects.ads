--  dh_ada_library, helper for Ada libraries Debian maintainers
--
--  Copyright (C) 2012-2020 Nicolas Boulenguez <nicolas@debian.org>
--
--  This program is free software: you can redistribute it and/or
--  modify it under the terms of the GNU General Public License as
--  published by the Free Software Foundation, either version 3 of the
--  License, or (at your option) any later version.
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
--  General Public License for more details.
--  You should have received a copy of the GNU General Public License
--  along with this program. If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Containers.Indefinite_Vectors;

package Projects is

   --  Encapsulate all GNAT project stuff handling.
   --  No attempt is done to clean memory.

   pragma Elaborate_Body;

   type Assignment (Name_Length  : Natural;
                    Value_Length : Natural) is
      record
         Name  : String (1 .. Name_Length);
         Value : String (1 .. Value_Length);
      end record;

   package Assignment_Vectors is
      new Ada.Containers.Indefinite_Vectors (Positive, Assignment);

   function Initialized return Boolean with Inline;

   procedure Parse
     (Assignments : in Assignment_Vectors.Vector;
      File_Name   : in String)
     with Post => Initialized;
   Parse_Error : exception;

   function Library_Name     return String  with Inline, Pre => Initialized;
   function Dynamic          return Boolean with Inline, Pre => Initialized;
   function Is_Library       return Boolean with Inline, Pre => Initialized;
   function Externally_Built return Boolean with Inline, Pre => Initialized;
   function Library_Dir      return String  with Inline, Pre => Initialized;
   --  Absolute path ending with a slash.

   procedure Iterate_On_Imported_Projects
     (Process : not null access procedure
        (Imported_Full_File_Name : in String;
         Imported_Library_Name   : in String))
     with Pre => Initialized;
   --  Imported_Library_Name is "" if the imported project is not a
   --  library project.

   procedure Iterate_On_Ali_Files
     --  Process each ALI file. Each Ada unit has an ALI file, named
     --  after the path of the compiled body, or specification in the
     --  absence of a body.
     --  Separate bodies do not have an ALI file.
     --  If the unit is found in a a name with an usual naming scheme,
     --  the ALI file is named after the actual file, not the replaced
     --  one. Renaming an ALI file is not wise, as they contain this
     --  actual name.
     (Process : not null access procedure (Path : in String))
     with Pre => Initialized;

   procedure Iterate_On_Linker_Options
     (Process : not null access procedure (Option : in String))
     with Pre => Initialized;
   --  Non-recursive list of Linker'Linker_Options for this project.

   procedure Iterate_On_Naming_Exceptions
     (Process : not null access procedure (Unit    : in String;
                                           File    : in String;
                                           Is_Body : in Boolean))
     with Pre => Initialized;
   --  Separate bodies are reported with Is_Body.

   procedure Iterate_On_Sources
     (Process : not null access procedure (Path   : in String;
                                           In_Ada : in Boolean))
     with Pre => Initialized;

end Projects;
