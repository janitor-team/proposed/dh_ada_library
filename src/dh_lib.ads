--  dh_ada_library, helper for Ada libraries Debian maintainers
--
--  Copyright (C) 2012-2020 Nicolas Boulenguez <nicolas@debian.org>
--
--  This program is free software: you can redistribute it and/or
--  modify it under the terms of the GNU General Public License as
--  published by the Free Software Foundation, either version 3 of the
--  License, or (at your option) any later version.
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
--  General Public License for more details.
--  You should have received a copy of the GNU General Public License
--  along with this program. If not, see <http://www.gnu.org/licenses/>.
--
with Ada.Command_Line;
with Ada.Environment_Variables;

with GNAT.OS_Lib;

with String_Sets;

package Dh_Lib is

   pragma Elaborate_Body;

   --  As close as possible to Debian::Debhelper::Dh_Lib perl library,
   --  see /usr/share/doc/debhelper/PROGRAMMING.gz.

   Verbose : Boolean := Ada.Environment_Variables.Exists ("DH_VERBOSE")
     and then Ada.Environment_Variables.Value ("DH_VERBOSE") /= "";

   Noact : Boolean := Ada.Environment_Variables.Exists ("DH_NOACT")
     and then Ada.Environment_Variables.Value ("DH_NOACT") /= "";

   Indep : Boolean := False;

   Dopackages : String_Sets.Set;

   type Command_Line_Argument_Set is
     array (1 .. Ada.Command_Line.Argument_Count) of Boolean;
   procedure Init
     (Ignored : in Command_Line_Argument_Set);
   Unknown_Command_Line_Argument : exception;

   procedure Do_It
     (Program_Name : in String;
      Args         : in GNAT.OS_Lib.Argument_List);
   pragma Precondition (Program_Name (Program_Name'First) = '/');
   --  It will run the command (unless no_act is set), and if Verbose
   --  is set, it will also output the command to stdout, without the
   --  directory part. You should use this procedure for almost all
   --  commands your program performs that manipulate files in the
   --  package build directories.

   --  May raise Subprocess_Error.

   procedure Verbose_Print (Message : in String);
   --  Echo Message with a leading horizontal tab if Verbose is set.

   procedure Warning (Message : in String);
   --  Output Message to standard error as a warning message.

   procedure Addsubstvar
     (Binary_Package                   : in String;
      Substvar                         : in String;
      Package_That_Will_Be_Depended_On : in String;
      Version_Info                     : in String := "");
   --  Append Debian substitution variables for binary packages
   --  control file generation (see deb-substvars(5)). In the
   --  debhelper way, each binary package has its own settings, stored
   --  in debian/packagename.substvars. Version_Info is ignored if
   --  empty. If provided, it should be similar to ">= 1.1".

   procedure Write_Log
     (Command        : in String := Ada.Command_Line.Command_Name;
      Binary_Package : in String);
   --  Writes the log files for the specified package, adding the
   --  command to the end.

   ----------------------------------------------------------------------

   --  Shortcuts avoiding to launch a separate subprocess for common
   --  operations. Displays readable relative paths, and be verbose
   --  only when the action would do something.

   procedure Create_Path (New_Directory : in String);
   --  Ada.Directories.Create_Path respecting Verbose and Noact.

   procedure Copy_File (Source_Name : in String;
                        Target_Name : in String);
   --  Ada.Directories.Copy_File respecting Verbose, Noact, and
   --  replacing the target if it already exists.

   --  Symbolic links being not handled very well by GNAT, even
   --  checking if one exists is hard. Use Do_It ("/bin/ln" ...).

end Dh_Lib;
